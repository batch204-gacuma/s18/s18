function add(num1, num2){

	let sum = num1+num2;
	console.log("The product of 15 and 5 is")
	console.log(sum);
   
}
add(5, 15);
function sub(num1, num2){

	let diff = num1-num2;
	console.log("The product of 20 and 5 is")
	console.log(diff);
   
}
sub(20, 5);

function product(num1, num2){
	console.log("the product of "+num1+" and "+num2);
	let result = num1*num2;
	return result;
}


let product1 = product(50, 10);
console.log (product1);

function quotient(num1, num2){

	console.log("the quotient of "+num1+" and "+num2);
	let result1 = num1/num2;
	return result1;

}
let quotient1 = quotient(50, 10);
console.log (quotient1);

function circleArea(radius) {
	
	console.log("the result of getting the area of a circle with 15 radius: ");
	const pi = 3.1416;
	let area = pi*radius**2
	return area;
	
}


let area1 = circleArea(15);
console.log(area1);

function average(num1, num2, num3, num4){

	console.log("the average of 20, 40, 60 and 80: ")
	let avg = (num1 + num2 + num3 + num4) / 4;
	return avg;

}
let avg1 = average(20, 40, 60, 80);
console.log(avg1);


function passing(num1, num2){

	console.log("Is "+num1+"/"+num2+" a passing score");
	let percentage = (num1 / num2) * 100;
	let isPassing = percentage >= 75;
	return isPassing;
}
let isPassing1 = passing(38, 50);
console.log(isPassing1);